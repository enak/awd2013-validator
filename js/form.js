$(function(){

    //load_states();
    
    $(".myform").submit(function(e){
        
        //for debugging purposes:
        //  e.preventDefault();
        
        var i=0,
        ret = true, 
        alert_string = "", 
        // originally implemeted using the bootstrap alert class
            // added flag to follow assignment specifications
        USING_ALERTS_DIV = false;
        
        // remove old form errors
        if (USING_ALERTS_DIV) $(".form-alert").remove();
        
        // check the "simply required fields"
        var simply_required = [$("#firstName"), $("#lastName"), $("#city"), $("#state")];
        for (i in simply_required) {
            var $field = simply_required[i];
            //console.log($field);
            if ($field.val() === null || $field.val().trim().length === 0) {
                var label = $("label[for=" + $field[0].name + "]").text();
                //$(".alert-container").append
                if(USING_ALERTS_DIV) $field.before('<div class="alert alert-danger form-alert">' + label + ' is a required field.</div>');
                else alert_string += label + ' is a required field.\n';
                
                ret = false;
            }
        }
        
        // check email field
        var $email = $("#email"),
        email_v = $("#email").val(),
        email_pattern = /^[a-zA-z0-9_\-.,]+@[a-zA-z0-9_\-.,]+\.[a-zA-z0-9_\-.,]/;
        
        console.log(email_v.trim());
        
        if (email_v.trim().length === 0) {
            if(USING_ALERTS_DIV) $email.before('<div class="alert alert-danger form-alert">Email Address is a required field.</div>');
            else alert_string += 'Email Address is a required field.\n';
            
            ret = false;
        } else if (!email_pattern.test(email_v)) {
            if(USING_ALERTS_DIV) $email.before('<div class="alert alert-danger form-alert">The email address, <code>' + email_v + '</code>, is not valid.</div>');
            else alert_string += 'The email address, ' + email_v + ', is not valid.\n';
            
            ret = false;
        }
        
        var $phone = $("#phone"), 
        phone_v = $("#phone").val(), 
        phone_pattern = /^(\([0-9]{3}\) [0-9]{3}[-]?[0-9]{4}|[0-9]{3}\-[0-9]{3}\-[0-9]{4}|[0-9]{10})$/;
        /*
        Acceptable formats
        5554441111
        555-444-1111
        (555) 444-1111
        (555) 4441111
        */
        
        if (phone_v.trim().length === 0) {
            // Not a required field.
            $phone.val("");
        } else if (!phone_pattern.test(phone_v)) {
            if(USING_ALERTS_DIV) $phone.before('<div class="alert alert-danger form-alert">The phone number, <code>' + phone_v + '</code>, is not valid. Please enter a 10 digit US phone number. Do no include the US dialing code, 1.</div>');
            else alert_string += 'The phone number, ' + phone_v + ', is not valid. Please enter a 10 digit US phone number. Do not include the US dialing code, 1.\n';
            
            ret = false;
        }
        
        // check the terms of service
        if ($("#terms").attr("disabled") !== undefined) {
            if(USING_ALERTS_DIV) $("#terms").before('<div class="alert alert-danger form-alert">You must agree to the terms of services by reading and checking off <i>"I agree to terms."</i>.</div>');
            else alert_string += 'You must agree to the terms of services by reading and checking off "I agree to terms.".';
            
            ret = false;
        } else if (!$("#terms").is(":checked")) {
            if(USING_ALERTS_DIV) $("#terms").before('<div class="alert alert-danger form-alert">You must agree to the terms of services checking off <i>"I agree to terms."</i>.</div>');
            else alert_string += 'You must agree to the terms of services checking off "I agree to terms.".\n';
            
            ret = false;
        }
        
        //for debugging purposes
        //if (ret) {
            //$(".alert-container").append('<div class="alert alert-success form-alert">Valid Form</div>');
        //}
        
        if (!USING_ALERTS_DIV && alert_string.length>0) alert(alert_string);
        return ret;
    });
    
    $(".terms_open").click(function(){
        $('#termsModal').modal('show');
    });
    
    // Add a listener that detects modal being closed.
    $('#termsModal').on('hidden.bs.modal', function () {
        $("#terms").removeAttr("disabled");
        $(".terms-help").remove();
    });    
    
    // Add a listener that detects modal after it is opened. 
        // It needs to check after it was opened because the height would not have been set otherwise.
    $('#termsModal').on('shown.bs.modal', function () {
        var $modal_body = $('#termsModal .modal-body'), 
        window_height = $(window).height(), 
        space_for_head_and_foot = 225;
        
        if($modal_body.height() + space_for_head_and_foot > window_height) {
            $modal_body.css("max-height", window_height - space_for_head_and_foot).css("overflow-y", "scroll");
            $('#termsModal').css("overflow-y", "auto");
        }
    });
    
    $(".terms-accept").click(function() {
        $('#termsModal').modal('hide'); //this will trigger the hide modal listener which enables the #terms checkbox.
        //$("#terms").attr("checked", "checked");
        $("#terms")[0].checked = true;
    });

});
/*
// I really wanted to load the states using javascript, but c9 doesn't always load this file properly.
function load_states() {
    for (i in states) {
        $("#state").append('<option value="' + states[i].abbreviation + '">' + states[i].name + "</option>");
    }
}
*/
