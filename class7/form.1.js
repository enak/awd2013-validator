
var form = document.getElementsByClassName("myform")[0];
var field = document.getElementById("firstName");

var validator = function(e) {
    if (field.value.trim() === "") {
        alert("You must enter a first name! It is all I ask from you!");
        return false;
    }
    return true;
};

form.onsubmit = validator;

var field_adder = function(e) {
    e.preventDefault();
    var input = document.createElement("input"), 
    new_name = document.getElementById("add_name");
    input.name = new_name.value;
    input.type = document.getElementById("add_type").value;
    input.classList.add("form-control");
    
    var label = document.createElement("label");
    label.innerText = document.getElementById("add_label").value;
    label.classList.add("control-label");
    label.classList.add("col-lg-2");
    
    var newInputDiv = document.createElement("div");
    newInputDiv.classList.add("col-lg-10");
    newInputDiv.appendChild(input);
    
    var newDiv = document.createElement("div");
    newDiv.classList.add("form-group");
    newDiv.appendChild(label);
    newDiv.appendChild(newInputDiv);
    
    form.appendChild(newDiv);
    
    var btn = document.getElementById("main-form-submit");
    form.appendChild(btn.parentElement.parentElement);
    
    return false;
};

var add_element_form = document.getElementById("add_element");
add_element_form.onsubmit = field_adder;