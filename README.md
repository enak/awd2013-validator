Stephen Kane
Advanced Web Design Technologies
Homework 2 - Form Validator

https://c9.io/skane9/awd2013-validator
https://c9.io/skane9/awd2013-validator/workspace/index.html

Please perform the task in a working Cloud 9 project and provide a 1-page write-up explaining IN BRIEF SENTENCES what techniques you used to implemented the following and why: 

validation, 
On page load, I applied a submit listener to the form. This listener uses regular expressions to check the email and phone number formats. For the required fields, I first run a trim function on them to get rid of white space. Then I check to see if their lengths are greater than 0. 

Originally, I had used Bootstrap’s alert class to inform the user of a mistake. This functionality is still in place, but I disabled it upon adding the alert() box which follows the assignment specifications. The alert box displays a concatenated string of validation errors.

terms of service checkbox behavior, 
The terms of services checkbox initially has the disabled attribute which prevents it from being clicked. 

I used the event, hidden.bs.modal, provided in the bootstrap documentation, to trigger the enabling of the checkbox. (It doesn’t matter how the user closes the terms modal.)

I added a click listener to the Accept button which first closes the dialog (which triggers the hidden.bs.modal and the listener that enables the checkbox) then sets the checkbox as checked.

and the modal dialog popup.
In addition to the events described in the terms of service area, I added a listener on the event, shown.bs.modal in order to change the behavior of the overflow(-y). When it is triggered, it checks if the dialog is taller than the window, then, if the answer is yes, it changes the overflow-y and max-height properties of the involved modal elements. The end result is a modal that fits in the window and has its own scrollbar.

I had to use shown instead of show event on the modal because I needed to wait for the the height property to have a value in it.

https://c9.io/mkhajuria13/javascript
https://c9.io/sbodi/homework2
